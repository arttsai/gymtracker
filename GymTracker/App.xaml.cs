﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using GymTracker.Services;
using GymTracker.Views;

namespace GymTracker
{
    public partial class App : Application
    {

        public App()
        {
            InitializeComponent();

            DependencyService.Register<MockDataStore>();
            MainPage = new NavigationPage(new SignInPage());
        }

        public void LoadMainPage()
        {
            MainPage = new MainPage();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
