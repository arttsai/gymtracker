﻿using System;
using System.Threading.Tasks;
using GymTracker.Models;
using GymTracker.ViewModels;

namespace GymTracker.Services
{
    public class WebServices
    {
        public WebServices()
        {
        }

        public static Task<bool> Authenticate(string Username, string Password)
        {
            Task<bool> submit = Task.Run(() =>
            {
                //make authentication call
                if (Username == "test" && Password == "password")
                    return true;
                return false;
            });

            return submit;
            
        }

        public static Task<Boolean> CreateNewAccount(AccountInfo user)
        {
            //make new account call
            return Task.FromResult(true);
        }
    }
}
